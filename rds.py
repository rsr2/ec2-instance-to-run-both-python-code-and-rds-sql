import pymysql
import config
import pandas as pd
#sql
import csv
import os
import mysql.connector
import time
#import subprocess
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
conn=pymysql.connect(
        host = config.host,
        user = config.user,
        password = config.passwd)
cursor=conn.cursor()
cursor.execute("select version()")
data=cursor.fetchone()
#sql= '''create database demographics'''
#cursor.execute(sql)

cursor.connection.commit()

sql= '''use demographics'''
cursor.execute(sql)
# sql= '''
# create table person4(
# id int not null auto_increment,
# name text,
# age int,
# sex text,
# primary key(id)
# )
# '''
# cursor.execute(sql)

def updatesqlperson(path):
    print(path)
    path=str(path)
    print(type(path))
    time.sleep(5)
    colnames=['Name','age','sex']
    df=pd.read_csv(path,names=colnames)
    print(df)
    df['sex']=df['sex'].astype(str)
    print(type(df.iloc[0]['sex']))
    print(df.dtypes)
    for i in range(0,len(df)):
        sql= '''
        insert into person(name,age,gender) values ('%s','%s',"%s")'''%(df.iloc[i]['Name'],df.iloc[i]['age'],str(df.iloc[i]['sex']))
        cursor.execute(sql)
        conn.commit()

def updatesqlorders(path):
    print(path)
    path=str(path)
    print(type(path))
    time.sleep(5)
    colnames=['OrderID','OrderNumber','PersonID']
    df2=pd.read_csv(path,names=colnames)
    print(df2)
    count=[]
    for i in range(0,len(df2)):
        sql='''select count(*) from person where id = '%s' '''%(df2.iloc[i]['PersonID'])
        cursor.execute(sql)
        count=cursor.fetchall()
        print(type(count[0][0]))
        print(count[0][0])
        if count[0][0]==1:
            sql= '''
            insert into Orders(OrderID,OrderNumber,PersonID) values ('%s','%s','%s')'''%(df2.iloc[i]['OrderID'],df2.iloc[i]['OrderNumber'],df2.iloc[i]['PersonID'])
            cursor.execute(sql)
            conn.commit()
        else:
            pass 


     if __name__ == "__main__":
    patterns = ["*"]
    ignore_patterns = None
    ignore_directories = False
    case_sensitive = True
    my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
    def on_created(event):
        print(f", {event.src_path} has been created!")
        print(event.src_path)
        p = str(event.src_path)
        p=p.split("\\")[-2]
        p=str(p)
        print(p)
        if p=='person':
            updatesqlperson(event.src_path)
        elif p=='orders':
            updatesqlorders(event.src_path)
            
        

    def on_deleted(event):
        print(f " {event.src_path} has been deleted !")

    def on_modified(event):
         print(f"{event.src_path} has been modified")

    def on_moved(event):
        print(f", {event.src_path} has been moved to {event.dest_path}")

    my_event_handler.on_created = on_created
    my_event_handler.on_deleted = on_deleted
    my_event_handler.on_modified = on_modified
    my_event_handler.on_moved = on_moved

    #path = "/home/ubuntu/person"
    #path1 = "/home/ubuntu/orders"
    #path = "C:/Users/rohit/Downloads/RDS-Python/person"
    #path1 = "C:/Users/rohit/Downloads/RDS-Python/orders"
    path=r"C:\Users\rohit\Downloads\RDS-Python\person"
    path1=r"C:\Users\rohit\Downloads\RDS-Python\orders"
    go_recursively = True
    my_observer = Observer()
    my_observer.schedule(my_event_handler, path, recursive=go_recursively)
    my_observer.schedule(my_event_handler, path1, recursive=go_recursively)

    my_observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        my_observer.stop()
        my_observer.join()
